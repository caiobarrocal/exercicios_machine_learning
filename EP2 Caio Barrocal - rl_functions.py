import numpy as np
from util import randomize_in_place


def linear_regression_prediction(X, w):
    """
    Calculates the linear regression prediction.

    :param X: design matrix
    :type X: np.ndarray(shape=(N, d))
    :param w: weights
    :type w: np.array(shape=(d, 1))
    :return: prediction
    :rtype: np.array(shape=(N, 1))
    """

    return X.dot(w)


def standardize(X):
    """
    Returns standardized version of the ndarray 'X'.

    :param X: input array
    :type X: np.ndarray(shape=(N, d))
    :return: standardized array
    :rtype: np.ndarray(shape=(N, d))
    """
    
    Xt = np.transpose(X)
    meansX = np.mean(X, axis=0)
    stdX = np.std(X, axis=0)
    

    X_out = np.divide(np.subtract(X, meansX), stdX)


    return X_out


def compute_cost(X, y, w):
    """
    Calculates  mean square error cost.

    :param X: design matrix
    :type X: np.ndarray(shape=(N, d))
    :param y: regression targets
    :type y: np.ndarray(shape=(N, 1))
    :param w: weights
    :type w: np.array(shape=(d,))
    :return: cost
    :rtype: float
    """

    #J(w) = 1/N(Xw - y)T(Xw-y)
    N = X.shape[0]

    Xw = np.matmul(X,w) #multiplicar X por w
    
    XwMinusY = np.subtract(Xw,y)
    
    J = (np.matmul(np.transpose(XwMinusY), XwMinusY))
    
    J = float(J)
    J = J/N
    
    return J


def compute_wgrad(X, y, w):
    """
    Calculates gradient of J(w) with respect to w.

    :param X: design matrix
    :type X: np.ndarray(shape=(N, d))
    :param y: regression targets
    :type y: np.ndarray(shape=(N, 1))
    :param w: weights
    :type w: np.array(shape=(d, 1))
    :return: gradient
    :rtype: np.array(shape=(d, 1))
    """
    
    N = X.shape[0]
    d = w.shape[0]

    yChapeu = np.matmul(X, w)
    yResult = np.transpose(np.subtract(yChapeu, y))
    
    grad = np.transpose(np.matmul(yResult, X)) 
    grad = np.multiply(np.divide(2,N), grad)

    return grad


def batch_gradient_descent(X, y, w, learning_rate, num_iters):
    """
     Performs batch gradient descent optimization.

    :param X: design matrix
    :type X: np.ndarray(shape=(N, d))
    :param y: regression targets
    :type y: np.ndarray(shape=(N, 1))
    :param w: weights
    :type w: np.array(shape=(d, 1))
    :param learning_rate: learning rate
    :type learning_rate: float
    :param num_iters: number of iterations
    :type num_iters: int
    :return: weights, weights history, cost history
    :rtype: np.array(shape=(d, 1)), list, list
    """
    
    weights_history = [w.flatten()]
    cost_history = [compute_cost(X, y, w)]
    
    for t in range(num_iters):
      
        gradT = compute_wgrad(X, y, w) #gradiente w atual
        
        w = np.subtract(w, np.multiply(learning_rate, gradT)) #caldulo do w futuro
        
        #atualização
        weights_history.append(w.flatten())
        cost_history.append(compute_cost(X, y, w))
   
    return w, weights_history, cost_history 

def stochastic_gradient_descent(X, y, w, learning_rate, num_iters, batch_size):
    """
     Performs stochastic gradient descent optimization

    :param X: design matrix
    :type X: np.ndarray(shape=(N, d))
    :param y: regression targets
    :type y: np.ndarray(shape=(N, 1))
    :param w: weights
    :type w: np.array(shape=(d, 1))
    :param learning_rate: learning rate
    :type learning_rate: float
    :param num_iters: number of iterations
    :type num_iters: int
    :param batch_size: size of the minibatch
    :type batch_size: int
    :return: weights, weights history, cost history
    :rtype: np.array(shape=(d, 1)), list, list
    """
    
    N = X.shape[0]
    weights_history = [w.flatten()]
    cost_history = [compute_cost(X, y, w)]
    
    
    for t in range(num_iters):
        
        random_rows = np.random.choice(N, batch_size, replace=False)
        Xm = X[random_rows]
        ym = y[random_rows]
       
        gradT = compute_wgrad(Xm, ym, w) #gradiente w atual
        
        w = np.subtract(w, np.multiply(learning_rate, gradT)) #caldulo do w futuro
        
        #atualização
        weights_history.append(w.flatten())
        cost_history.append(compute_cost(X, y, w))
   
    return w, weights_history, cost_history 

