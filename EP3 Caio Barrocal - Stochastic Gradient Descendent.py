import torch.nn.functional as F
import numpy as np
import torch
from util import randomize_in_place


def sigmoid(x):
    return 1 / (1 + np.exp(-x))


def graph1(a_np, b_np, c_np):
    """
    Computes the graph
        - x = a * c
        - y = a + b
        - f = x / y

    Computes also df/da using
        - Pytorchs's automatic differentiation (auto_grad)
        - user's implementation of the gradient (user_grad)

    :param a_np: input variable a
    :type a_np: np.ndarray(shape=(1,), dtype=float64)
    :param b_np: input variable b
    :type b_np: np.ndarray(shape=(1,), dtype=float64)
    :param c_np: input variable c
    :type c_np: np.ndarray(shape=(1,), dtype=float64)
    :return: f, auto_grad, user_grad
    :rtype: torch.DoubleTensor(shape=[1]),
            torch.DoubleTensor(shape=[1]),
            numpy.float64
    """
    a = torch.from_numpy(a_np)
    a.requires_grad = True
    b = torch.from_numpy(b_np)
    c = torch.from_numpy(c_np)
    
    x = a * c
    y = a + b
    f = x / y
    
    #auto_grad
    f.backward()
    auto_grad = a.grad
    
    #user_grad 
    
    x_np = a_np * c_np
    y_np = a_np + b_np
    f_np = x_np / y_np
    
    df_dx = 1/y_np
    dx_da = c_np
    df_da = df_dx*dx_da
    
    df_dy = -1*((x_np)/(y_np**2))
    dy_da = 1
    df_da_y = df_dy*dy_da
    
    user_grad = df_da + df_da_y

    return f, auto_grad, user_grad

import torch.nn.functional as F

def graph2(W_np, x_np, b_np):
    """
    Computes the graph
        - u = Wx + b
        - g = sigmoid(u)
        - f = sum(g)

    Computes also df/dW using
        - pytorchs's automatic differentiation (auto_grad)
        - user's own manual differentiation (user_grad)
        
    F.sigmoid may be useful here

    :param W_np: input variable W
    :type W_np: np.ndarray(shape=(d,d), dtype=float64)
    :param x_np: input variable x
    :type x_np: np.ndarray(shape=(d,1), dtype=float64)
    :param b_np: input variable b
    :type b_np: np.ndarray(shape=(d,1), dtype=float64)
    :return: f, auto_grad, user_grad
    :rtype: torch.DoubleTensor(shape=[1]),
            torch.DoubleTensor(shape=[d, d]),
            np.ndarray(shape=(d,d), dtype=float64)
    """
    
    w = torch.from_numpy(W_np)
    w.requires_grad = True
    b = torch.from_numpy(b_np)
    x = torch.from_numpy(x_np)

    u = torch.matmul(w,x) + b
    
    #sigmoid
    g = F.sigmoid(u)
    f = torch.sum(g)
    
    #auto_grad
    f.backward()
    auto_grad = w.grad
    
    #user_grad
    u_np = np.matmul(W_np, x_np) + b_np
    g_np = sigmoid(u_np)
    f_np = np.sum(g_np)
    
    d = g_np.shape[0]
    
    #derivadas parciais
    df_dg = np.ones((1, d))
    dg_du = np.multiply(g_np, np.subtract(np.ones_like(g_np), g_np))
    df_du = np.matmul(dg_du,df_dg)
    
    
    du_dw = np.zeros((d,d,d))
    for i in range (0,d):
        np.fill_diagonal(du_dw[:][:][i], x_np[i][0])
    
    
    du_dw_df_du = np.matmul(du_dw, df_du)
    
    user_grad = np.zeros((d,d))
    for k in range(0,d):
        user_grad[:][k] = du_dw_df_du[:][:][k].diagonal()
    
    user_grad = np.transpose(user_grad)
    
    return f, auto_grad, user_grad


def SGD_with_momentum(X, y, inital_w, iterations, batch_size, learning_rate, momentum):
    """
    Performs batch gradient descent optimization using momentum.

    :param X: design matrix
    :type X: np.ndarray(shape=(N, d))
    :param y: regression targets
    :type y: np.ndarray(shape=(N, 1))
    :param inital_w: initial weights
    :type inital_w: np.array(shape=(d, 1))
    :param iterations: number of iterations
    :type iterations: int
    :param batch_size: size of the minibatch
    :type batch_size: int
    :param learning_rate: learning rate
    :type learning_rate: float
    :param momentum: accelerate parameter
    :type momentum: float
    :return: weights, weights history, cost history
    :rtype: np.array(shape=(d, 1)), list, list
    """
    N = X.shape[0]
    
    weights_history = []
    cost_history = []
    w0 = inital_w
    
    X = torch.from_numpy(X)
    y = torch.from_numpy(y)
    
    z = np.zeros_like(inital_w)

    for t in range(iterations):
        
        #selecionando aleatoriamente
        random_rows = np.random.choice(N, batch_size, replace=False)
        Xm = X[random_rows]
        ym = y[random_rows]
        
        #criando tensor de pesos
        weights_history.append(w0.flatten()) #atualizando lista de pesos
        w = torch.from_numpy(w0)
        w.requires_grad = True
        
        #custo gaussiano
        j = torch.sum((torch.matmul(Xm,w) - ym)**2)/batch_size 
        j.backward()
        cost_history.append(j) #atualização de custo
        
        #vetor de estados
        z = momentum*z + w.grad
        
        #calculando novos pesos
        w = w - learning_rate*z
        w0 = w.detach().numpy()
        
        
        
          
        
    w_np = w.data.numpy()
    
    return w_np, weights_history, cost_history
