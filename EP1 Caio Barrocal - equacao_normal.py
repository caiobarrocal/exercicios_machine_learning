#8941130

import numpy as np

def normal_equation_prediction(X, y):
    """
    Calculates the prediction using the normal equation method.
    You should add a new column with 1s.

    :param X: design matrix
    :type X: np.array
    :param y: regression targets
    :type y: np.array
    :return: prediction
    :rtype: np.array
    """

    N = len(X)
    d = len(X[0]) + 1
    
    x = np.c_[(np.ones(N), X)]
    
    xt = np.transpose(x)
    
    xPseudo = np.matmul(xt, x) #multiplicar XTransposta por X
    xPseudo = np.linalg.inv(xPseudo) #inverter a multiplicação anterior
    xPseudo = np.matmul(xPseudo,xt)
    
    w = np.matmul(xPseudo,y)

    
    # PREDICTION
    
    prediction = np.matmul(x, w)
        
    return prediction
   

try:
    X, y = get_hausing_prices_data(N=100)
    prediction = normal_equation_prediction(X, y) 
    plot_points_regression(X,
                           y,
                           title='Real estate prices prediction',
                           xlabel="m\u00b2",
                           ylabel='$',
                           prediction=prediction,
                           legend=True)
except NotImplementedError:
    print("Falta fazer!")
