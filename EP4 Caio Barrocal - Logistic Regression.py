import os
import subprocess
import numpy as np
import torch
import torch.nn as nn
import matplotlib.pyplot as plt


class LogisticRegression(nn.Module):
    """
    Logistic regression model.
    
    You may find nn.Linear and nn.Softmax useful here.
    
    :param config: hyper params configuration
    :type config: LRConfig
    """
    def __init__(self, config):
        super(LogisticRegression, self).__init__()
        
        n_features = (config.width)*(config.height)*(config.channels)
        self.linear = nn.Linear(n_features, config.classes)

    def forward(self, x):
        """
        Computes forward pass

        :param x: input tensor
        :type x: torch.FloatTensor(shape=(batch_size, number_of_features))
        :return: logits
        :rtype: torch.FloatTensor(shape=[batch_size, number_of_classes])
        """
        logits = self.linear(x)
        return logits
    
    
    def predict(self, x):
        """
        Computes model's prediction

        :param x: input tensor
        :type x: torch.FloatTensor(shape=(batch_size, number_of_features))
        :return: model's predictions
        :rtype: torch.LongTensor(shape=[batch_size])
        """

        logits = self.forward(x)
        s = nn.Softmax(dim = 1)
        preds = s(logits)
        predictions = torch.argmax(preds, dim=1)
        
        return predictions


class DFN(nn.Module):
    """
    Deep Feedforward Network.
    
    The method self._modules is useful here.
    The class nn.ReLU() is useful too.
    
    :param config: hyper params configuration
    :type config: DFNConfig
    """
    def __init__(self, config):
        super(DFN, self).__init__()
        
        d0 = config.height*config.width*config.channels
        
        for i in range(0, len(config.architecture)):
            
            d1 = config.architecture[i]
            
            self.add_module(str(i), nn.Linear(d0, d1))
            d0 = d1

    def forward(self, x):
        """
        Computes forward pass

        :param x: input tensor
        :type x: torch.FloatTensor(shape=(batch_size, number_of_features))
        :return: logits
        :rtype: torch.FloatTensor(shape=[batch_size, number_of_classes])
        """
        for module in self._modules.values():
            
            r = nn.ReLU()
            x = r(module(x))
            
        logits = x
        return logits
        
    
    def predict(self, x):
        """
        Computes model's prediction

        :param x: input tensor
        :type x: torch.FloatTensor(shape=(batch_size, number_of_features))
        :return: model's predictions
        :rtype: torch.LongTensor(shape=[batch_size])
        """
        logits = self.forward(x)
        s = nn.Softmax(dim = 1)
        preds = s(logits)
        predictions = torch.argmax(preds, dim=1)
        
        return predictions      


def train_model_img_classification(model,
                                   config,
                                   dataholder,
                                   model_path,
                                   verbose=True):
    """
    Train a model for image classification

    :param model: image classification model
    :type model: LogisticRegression or DFN
    :param config: image classification model
    :type config: LogisticRegression or DFN
    :param dataholder: data
    :type dataholder: DataHolder
    :param model_path: path to save model params
    :type model_path: str
    :param verbose: param to control print
    :type verbose: bool
    """
    train_loader = dataholder.train_loader
    valid_loader = dataholder.valid_loader

    best_valid_loss = float("inf")
    
    # i) define the loss criteria and the optimizer. 
    # You may find nn.CrossEntropyLoss and torch.optim.SGD useful here.
    
    criterion = nn.CrossEntropyLoss()
    optimizer = torch.optim.SGD(model.parameters(), lr = config.learning_rate, momentum = config.momentum)

    train_loss = []
    valid_loss = []
    
    for epoch in range(config.epochs):
        for step, (images, labels) in enumerate(train_loader):
            images = images/255
            # ii) You should zero the model gradients

            optimizer.zero_grad()
            loss = criterion(model(images), labels)

            if step % config.save_step == 0:
                
                # iii) You should define the loss function for the valid data.
                v_images, v_labels = next(iter(valid_loader))
                v_images = v_images/255
                
                v_loss = criterion(model(v_images), v_labels)
                
                valid_loss.append(float(v_loss))
                train_loss.append(float(loss))
                if float(v_loss) < best_valid_loss:
                    msg = "\ntrain_loss = {:.3f} | valid_loss = {:.3f}".format(float(loss),float(v_loss))
                    torch.save(model.state_dict(), model_path)
                    best_valid_loss = float(v_loss)
                    if verbose:
                        print(msg, end="")
    
            # iv) You should do the back propagation
            # and do the optimization step.
            loss.backward()
            optimizer.step()
    
    if verbose:
        x = np.arange(1, len(train_loss) + 1, 1)
        fig, ax = plt.subplots(1, 1, figsize=(12, 5))
        ax.plot(x, train_loss, label='train loss')
        ax.plot(x, valid_loss, label='valid loss')
        ax.legend()
        plt.xlabel('step')
        plt.ylabel('loss')
        plt.title('Train and valid loss')
        plt.grid(True)
        plt.show()

